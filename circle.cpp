#include <circle.hpp>

namespace saw {
	Circle::Circle() {

	}

	Circle::Circle(int R) {

		m_R = R;
		if (R <= 0)
		{
			std::cout << "R<=0" << std::endl;
			m_R = 1;
		}

	}
	Circle::~Circle() {
	}


	double Circle::Square() {
		return m_R * m_R * PI;
	}

	double Circle::Len() {
		return 2 * m_R * PI;
	}

	void Circle::SetR(int R) {
		m_R = R;
		if (R <= 0)
		{
			std::cout << "R<=0" << std::endl;
			m_R = 1;
		}
	}

	int Circle::GetR() {
		return m_R;
	}

}