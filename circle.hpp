#pragma once
#include<iostream>
#define PI acos(-1)

namespace saw {
	class Circle {
	public:
		Circle();
		Circle(int R);
		~Circle();
		double Square();
		double Len();
		void SetR(int R);
		int GetR();
	private:
		int m_R;
	};
}